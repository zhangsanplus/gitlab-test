FROM node:14.15.1 as builder
WORKDIR /app
COPY . .
# RUN npm install --registry=http://registry.npm.taobao.org
# RUN npm run build

FROM nginx:latest
COPY --from=builder /app/dist /usr/share/nginx/html

